/** @format */

import React from "react"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"
import { Testimonial } from "./testimonial"

export const TestimonialExample = () => (
  <ThemeProvider>
    <Testimonial
      active
      data={{
        name: "Barbra",
        description: "head of barbering",
        content: "Great scissors!",
        avatar: "https://static.bit.dev/bit-logo.png",
      }}
      data-testid="test-testimonial"
    />
  </ThemeProvider>
)
