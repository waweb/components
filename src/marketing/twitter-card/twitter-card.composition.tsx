/** @format */

import React from "react"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"
import { TwitterCard } from "./twitter-card"

export const TwitterCardExample = () => (
  <ThemeProvider style={{ maxWidth: 250 }}>
    <TwitterCard
      title="@bitdev_"
      image="bit-logo.png"
      verified={true}
      data-testid="test-twitter-card"
    >
      This is my tweet
    </TwitterCard>
  </ThemeProvider>
)
