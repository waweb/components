/** @format */

import React from "react"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"
import { brands } from "@waweb/uikit.theme.brands"
import { GithubLink } from "./github-link"

export const GithubLinkExample = () => (
  <ThemeProvider className={brands}>
    <GithubLink
      href="https://github.com/teambit/bit"
      starCount={500000}
      data-testid="test-link"
    />
  </ThemeProvider>
)

GithubLinkExample.canvas = {
  height: 90,
}
