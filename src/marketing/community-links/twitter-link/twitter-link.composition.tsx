/** @format */

import React from "react"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"
import { brands } from "@waweb/uikit.theme.brands"
import { TwitterLink } from "./twitter-link"

export const TwitterLinkExample = () => (
  <ThemeProvider className={brands}>
    <TwitterLink href="https://twitter.com/bitdev_" data-testid="test-link" />
  </ThemeProvider>
)

TwitterLinkExample.canvas = {
  height: 90,
}
