/** @format */

import React from "react"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"
import { brands } from "@waweb/uikit.theme.brands"
import { SlackLink } from "./slack-link"

export const SlackLinkExample = () => (
  <ThemeProvider className={brands}>
    <SlackLink
      href="https://join.slack.com/t/bit-dev-community/shared_invite/enQtNzM2NzQ3MTQzMTg3LWI2YmFmZjQwMTkxNmFmNTVkYzU2MGI2YjgwMmJlZDdkNWVhOGIzZDFlYjg4MGRmOTM4ODAxNTIxMTMwNWVhMzg"
      data-testid="test-link"
    />
  </ThemeProvider>
)

SlackLinkExample.canvas = {
  height: 90,
}
