/** @format */

import React from "react"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"
import { brands } from "@waweb/uikit.theme.brands"
import { CommunityLink } from "./community-link"

export const BaseCommunityLink = () => (
  <ThemeProvider className={brands}>
    <CommunityLink href="https://watheia.app" data-testid="test-link" external>
      watheia.app
    </CommunityLink>
  </ThemeProvider>
)

BaseCommunityLink.canvas = {
  height: 90,
}
