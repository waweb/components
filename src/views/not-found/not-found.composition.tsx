/** @format */

import React from "react"
import { NotFound } from "./not-found"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"

export const WithLightTheme = () => (
  <ThemeProvider data-testid="with-light-theme" colorScheme="light">
    <NotFound />
  </ThemeProvider>
)

export const WithDarkTheme = () => (
  <ThemeProvider data-testid="with-dark-theme" colorScheme="dark">
    <NotFound />
  </ThemeProvider>
)
