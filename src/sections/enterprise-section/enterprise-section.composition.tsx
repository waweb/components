/** @format */

import React from "react"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"
import { EnterpriseSection } from "./enterprise-section"

export const EnterpriseSectionExample = () => (
  <ThemeProvider>
    <EnterpriseSection data-testid="test-enterprise" />
  </ThemeProvider>
)

EnterpriseSectionExample.canvas = {
  width: 1400,
  height: 400,
  overflow: "scroll",
}
