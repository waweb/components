---
labels: ["react", "typescript", "home page", "components"]
description: "home page breaks down section"
---

<!-- @format -->

### Overview

A section of static content, showing how Watheia Labs breaks down complicated websites.  
Assumes the consuming component to supply className with width and other styles.  
[Link to page](https://watheia.app).
