/** @format */

import React from "react"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"
import { DevelopComponents } from "./develop-components"

export const DevelopComponentsExample = () => (
  <ThemeProvider>
    <DevelopComponents data-testid="test-develop" />
  </ThemeProvider>
)

DevelopComponentsExample.canvas = {
  width: 1400,
  height: 400,
  overflow: "scroll",
}
