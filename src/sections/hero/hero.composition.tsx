/** @format */

import React from "react"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"
import { Hero } from "./hero"

export const HeroExample = () => (
  <ThemeProvider>
    <Hero data-testid="test-hero" />
  </ThemeProvider>
)

HeroExample.canvas = {
  width: 1400,
  height: 600,
  overflow: "scroll",
}
