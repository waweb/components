import { ViewGenAspect } from './view-gen.aspect';

export type { ViewGenMain } from './view-gen.main.runtime';
export default ViewGenAspect;
export { ViewGenAspect };
